from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from . import models
from django.db.models import Sum


@login_required
def list_view(request):
    context = dict()
    context["orders"] = list(models.Order.objects.filter(closed=False)
                             .values("id", "item__name", "price", "total_quantity", "total_value", ).annotate(billed=Sum("lines__proforma_order_lines__quantity"))
                             .order_by('id'))
    return render(request=request, template_name="salesOrder/sales_list.html", context=context)
