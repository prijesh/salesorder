from django.db import models


class Order(models.Model):
    product = models.CharField(on_delete=models.CASCADE, related_name="order_lines")
    price = models.DecimalField(decimal_places=2, max_digits=8, default=0)
    quantity = models.DecimalField(decimal_places=2, max_digits=8, default=0)
    total_value = models.DecimalField(decimal_places=2, max_digits=8, default=0)

    def __str__(self):
        return "{}".format(self.product)
